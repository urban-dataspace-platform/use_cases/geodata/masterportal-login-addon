import Vuex from "vuex";
import {config, shallowMount, createLocalVue} from "@vue/test-utils";

import LoginComponent from "../../../components/Login.vue";
import Login from "../../../store/indexLogin";
import rootGetters from "../../../../../src/app-store/getters.js";
import Cookie from "../../../utils/utilsCookies";

import {expect} from "chai";
import sinon from "sinon";

const localVue = createLocalVue();

localVue.use(Vuex);

config.mocks.$t = key => key;

describe("ADDONS: addons/login/components/Login.vue", () => {
    const
        sandbox = sinon.createSandbox(),
        mockConfigJson = {
            Portalconfig: {
                menu: {
                    tools: {
                        children: {
                            loginComponent: {
                                oidcAuthorizationEndpoint: "https://idm.localhost",
                                oidcTokenEndpoint: "https://idm.localhost",
                                oidcClientId: "public_masterportal",
                                oidcRedirectUri: "https://localhost/portal/basic/",
                                oidcScope: "profile email openid",
                                interceptorUrlRegex: "https?://localhost/"
                            }
                        }
                    }
                }
            }
        };
    let store,
        wrapper;

    beforeEach(() => {
        store = new Vuex.Store({
            namespaces: true,
            modules: {
                Tools: {
                    namespaced: true,
                    modules: {
                        Login: Login
                    }
                }
            },
            getters: {
                mobile: () => false,
                ...rootGetters
            },
            state: {
                configJson: mockConfigJson
            }
        });
        store.commit("Tools/Login/setActive", true);
    });
    afterEach(() => {
        sinon.restore();
        if (wrapper) {
            wrapper.destroy();
        }
    });

    describe("Login template", () => {
        it("should exist", async () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.exists()).to.be.true;
        });

        it("should find Tool component", async () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});
            const toolWrapper = wrapper.findComponent({name: "ToolTemplate"});

            expect(toolWrapper.exists()).to.be.true;
        });

        it("should render Login", () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.find(".login-window[title=Login]").exists()).to.be.true;
            expect(wrapper.find(".login-window[title=Login] button#logout-button").exists()).to.be.true;
        });

        it("should have values from store Login renders", () => {
            store.commit("Tools/Login/setScreenName", "Max Mustermann");
            store.commit("Tools/Login/setEmail", "Max.Mustermann@domain.com");

            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.html().includes("Max Mustermann")).to.be.true;
            expect(wrapper.html().includes("Max.Mustermann@domain.com")).to.be.true;
        });

        it("should not render if active is false", () => {
            store.commit("Tools/Login/setActive", false);

            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.find(".login-window[title=Login]").exists()).to.be.false;
        });

        it("should not call logout fn if button was not clicked", () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});
            wrapper.vm.logout = sinon.fake();

            expect(wrapper.vm.logout.calledOnce).to.be.false;
        });

        it("should call logout fn if button is clicked", async () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});

            wrapper.vm.reload = sinon.fake();
            sandbox.spy(wrapper.vm, "logout");
            sandbox.spy(wrapper.vm, "close");

            await wrapper.find(".login-window[title=Login] button#logout-button").trigger("click");
            expect(wrapper.vm.logout.calledOnce).to.be.true;
            expect(wrapper.vm.close.calledOnce).to.be.true;
            expect(wrapper.vm.reload.calledOnce).to.be.true;
        });

        it("should close tool if logout button is clicked", async () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});
            wrapper.vm.reload = sinon.fake();

            await wrapper.find(".login-window[title=Login] button#logout-button").trigger("click");

            expect(wrapper.find(".login-window[title=Login]").exists()).to.be.false;
        });

        it("should call login after Login renders", () => {
            const spyLogin = sinon.spy(LoginComponent.methods, "checkLoggedIn");

            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(spyLogin.calledOnce).to.be.true;
        });

        it("should not be logged in after Login renders", () => {
            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.vm.checkLoggedIn()).to.be.false;
        });

        it("should be logged in after Login renders", () => {
            // return a "valid" cookie
            sinon.replace(Cookie, "get", sinon.fake.returns("fake value"));

            // fake that the token is not expired yet
            sinon.replace(LoginComponent.methods, "getTokenExpiry", sinon.fake.returns(1));

            wrapper = shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.vm.checkLoggedIn()).to.be.true;
        }).timeout(5000);

        it("should have values from cookies after Login renders", async () => {
            // return a "valid" cookie
            sinon.replace(Cookie, "get", sinon.fake.returns("fake value"));

            // fake that the token is not expired yet
            sinon.replace(LoginComponent.methods, "getTokenExpiry", sinon.fake.returns(1));

            wrapper = await shallowMount(LoginComponent, {store, localVue});

            expect(wrapper.vm.$store.state.Tools.Login.screenName).to.be.equal("fake value");
            expect(wrapper.vm.$store.state.Tools.Login.username).to.be.equal("fake value");
            expect(wrapper.vm.$store.state.Tools.Login.email).to.be.equal("fake value");
        }).timeout(5000);

    });

});


